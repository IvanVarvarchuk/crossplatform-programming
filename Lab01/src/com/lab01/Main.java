package com.lab01;

import model.ChassisType;
import model.CombatVehicle;
import model.CombatVehicleManager;
import model.CombatVehiclesCash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
	// write your code here
        var vm = new CombatVehicleManager();

        vm.shuffle();

        vm.getVehicles().forEach(System.out::print);

        
    }
}
