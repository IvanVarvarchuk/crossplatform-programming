package test;

import model.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class CombatVehicleManagerTest {

    @Test
    public void findAll() {
        //assign
        Map<String, Integer> config = new HashMap<>();
        config.put("T-84", 3);
        config.put("BTR-4", 4);
        config.put("BBM-5", 1);
        config.put("BBM-8", 5);

        CombatVehicleManager vehicleManager = new CombatVehicleManager(config);

        config.remove("T-84");
        config.remove("BTR-4");

        ArrayList<CombatVehicle> shouldBe = new ArrayList<>(new CombatVehicleManager(config).getVehicles());

        Predicate<CombatVehicle> isWheeled = v -> v.getChassisType() == ChassisType.WHEELED;
        Predicate<CombatVehicle> minConsumption = v -> v.getFuelConsumption() <= 20;

        //act
        vehicleManager.setVehicles(vehicleManager.findAll(isWheeled));
        vehicleManager.setVehicles(vehicleManager.findAll(minConsumption));

        //assert
        Assert.assertEquals(shouldBe, vehicleManager.getVehicles());

    }

    @Test
    public void sortByPersonnelCount() {
        //assign
        CombatVehiclesCash vehiclesCash = new CombatVehiclesCash();
        ArrayList<CombatVehicle> list = new ArrayList<>();

        list.addAll(vehiclesCash.get("T-84", 3));
        list.addAll(vehiclesCash.get("T-64E", 2));
        list.addAll(vehiclesCash.get("BREM-4", 1));
        list.addAll(vehiclesCash.get("BBM-5", 2));
        list.addAll(vehiclesCash.get("BBM-8", 5));
        list.addAll(vehiclesCash.get("BTR-4", 3));

        CombatVehicleManager vehicleManager = new CombatVehicleManager(list);

        ArrayList<CombatVehicle> shouldBe = (ArrayList<CombatVehicle>)vehicleManager.getVehicles().clone();
        vehicleManager.shuffle();
        //act
        vehicleManager.sortByPersonnelCount(CmpOrder.ASC);
        //vehicleManager.getVehicles().forEach(System.out::println);
        //assert
        Assert.assertEquals(shouldBe, vehicleManager.getVehicles());
    }

    @Test
    public void sortByArmorType() {
        //assign
        CombatVehiclesCash vehiclesCash = new CombatVehiclesCash();
        ArrayList<CombatVehicle> list = new ArrayList<>();

        list.add(vehiclesCash.get("T-64E"));
        list.add(vehiclesCash.get("BTR-4"));
        list.add(vehiclesCash.get("BBM-5"));

        CombatVehicleManager vehicleManager = new CombatVehicleManager(list);

        ArrayList<CombatVehicle> shouldBe = (ArrayList<CombatVehicle>)vehicleManager.getVehicles().clone();
        vehicleManager.shuffle();

        //act
        vehicleManager.sortByArmorType(CmpOrder.DSC);

        //assert
        Assert.assertEquals(shouldBe, vehicleManager.getVehicles());

    }

    @Test
    public void sortByMaxSpeed() {
        //assign
        CombatVehiclesCash vehiclesCash = new CombatVehiclesCash();
        ArrayList<CombatVehicle> list = new ArrayList<>();

        list.add(vehiclesCash.get("BREM-4"));
        list.add(vehiclesCash.get("BTR-4"));
        list.add(vehiclesCash.get("BBM-5"));
        list.add(vehiclesCash.get("T-64E"));

        CombatVehicleManager vehicleManager = new CombatVehicleManager(list);

        ArrayList<CombatVehicle> shouldBe = (ArrayList<CombatVehicle>)vehicleManager.getVehicles().clone();
        vehicleManager.shuffle();

        //act
        vehicleManager.sortByMaxSpeed(CmpOrder.DSC);

        //assert
        Assert.assertEquals(shouldBe, vehicleManager.getVehicles());

    }

    @Test
    public void sortByFuelConsumption() {
        //assign
        CombatVehiclesCash vehiclesCash = new CombatVehiclesCash();
        ArrayList<CombatVehicle> list = new ArrayList<>();

        list.addAll(vehiclesCash.get("T-64E", 2));
        list.addAll(vehiclesCash.get("T-84", 3));
        list.addAll(vehiclesCash.get("BREM-4", 1));
        list.addAll(vehiclesCash.get("BTR-4", 3));
        list.addAll(vehiclesCash.get("BBM-8", 5));
        list.addAll(vehiclesCash.get("BBM-5", 2));

        CombatVehicleManager vehicleManager = new CombatVehicleManager(list);

        ArrayList<CombatVehicle> expected = (ArrayList<CombatVehicle>)vehicleManager.getVehicles().clone();
        vehicleManager.shuffle();

        //act
        vehicleManager.sortByFuelConsumption(CmpOrder.DSC);

        //assert
        Assert.assertEquals(expected, vehicleManager.getVehicles());
    }
}