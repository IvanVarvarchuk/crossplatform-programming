package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CombatVehiclesCash {
    private Map<String, CombatVehicle> prototypes;

    public CombatVehiclesCash() {
        List<CombatVehicle> objects = List.of(
                new Tank("T-84", 70, 700, 3, 125,
                        ArmorType.HEAVY, ChassisType.FULL_TRACK, new Weapon(120, 40)),
                new Tank("T-64E", 60, 800, 4, 145,
                        ArmorType.HEAVY, ChassisType.FULL_TRACK, new Weapon(125, 36)),
                new CombatVehicle("BTR-4", 95, 900,10,90,
                        ArmorType.MEDIUM,ChassisType.WHEELED),
                new RecoveryVehicle("BREM-4", 100, 670,5,95,
                        ArmorType.MEDIUM,ChassisType.FULL_TRACK, 3000),
                new CombatVehicle("BBM-8", 110, 900,8,20,
                        ArmorType.LIGHT,ChassisType.WHEELED),
                new CombatVehicle("BBM-5", 90, 600,6,18,
                        ArmorType.LIGHT,ChassisType.WHEELED)

        );
        prototypes = new HashMap<>();

        objects.forEach(v -> prototypes.put(v.getName(), v));
        /*
        for (var item: objects) {
            prototypes.put(item.getName(), item);
        }*/

    }

    public ArrayList<CombatVehicle> get(String name, int count) {
        var  result = new ArrayList<CombatVehicle>();
        for (int i = count; i > 0; i--) {
            result.add(prototypes.get(name).clone());
        }
        return result;
    }

    public CombatVehicle get(String name) {
        return prototypes.get(name).clone();
    }
    public void put(CombatVehicle vehicle) {
        prototypes.put(vehicle.getName(), vehicle);
    }
}

