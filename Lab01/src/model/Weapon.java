package model;

public class Weapon {
    public int caliber;
    public int ammunition;

    public Weapon(int caliber, int ammunition) {
        this.caliber = caliber;
        this.ammunition = ammunition;
    }

    @Override
    public boolean equals(Object obj) {
        var wepon = (Weapon)obj;
        return caliber == wepon.caliber &&
                ammunition == wepon.ammunition;
    }
}
