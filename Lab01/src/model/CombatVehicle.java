package model;

import java.io.Serializable;
import java.util.Collections;

public class CombatVehicle implements Serializable {

    protected String name;
    protected int maxSpeed;
    protected int fuelCapacity;
    protected int personnelCount;
    protected int fuelConsumption ;
    protected ArmorType armorType;
    protected ChassisType chassisType;



    public CombatVehicle(String name, int maxSpeed, int fuelCapacity, int personnelCount,
                         int fuelConsumption, ArmorType armorType, ChassisType chassisType) {
        this.name = name;
        this.maxSpeed = maxSpeed;
        this.fuelCapacity = fuelCapacity;
        this.personnelCount = personnelCount;
        this.fuelConsumption = fuelConsumption;
        this.armorType = armorType;
        this.chassisType = chassisType;
    }

    public CombatVehicle(CombatVehicle combatVehicle) {
        this.name = combatVehicle.name;
        this.maxSpeed = combatVehicle.maxSpeed;
        this.fuelCapacity = combatVehicle.fuelCapacity;
        this.personnelCount = combatVehicle.personnelCount;
        this.fuelConsumption = combatVehicle.fuelConsumption;
        this.chassisType = combatVehicle.chassisType;
        this.armorType = combatVehicle.armorType;
    }

    public CombatVehicle clone() {
        return new CombatVehicle(this);
    }
/**/
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof CombatVehicle || obj instanceof Tank || obj instanceof RecoveryVehicle))
            return false;

        CombatVehicle temp = (CombatVehicle) obj;
        return  this.name.equals(temp.name) &
                this.maxSpeed == temp.getMaxSpeed() &&
                this.fuelCapacity == temp.getFuelCapacity() &&
                this.personnelCount == temp.getPersonnelCount() &&
                this.fuelConsumption == temp.getFuelConsumption() &&
                this.armorType == temp.getArmorType() &&
                this.chassisType == temp.getChassisType();
    }
/**/
    @Override
    public String toString() {

        return name + " maxSpeed: " + maxSpeed +
                " fuelCapacity:" + fuelCapacity +
                " personnelCount:" + personnelCount +
                " fuelConsumption" + fuelConsumption +
                " armorType:" + armorType +
                " chassisType:" + chassisType ;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getPersonnelCount() {
        return personnelCount;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public int getFuelCapacity() {
        return fuelCapacity;
    }

    public String getName() {
        return name;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public ChassisType getChassisType() {
        return chassisType;
    }

}
