package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.function.Predicate;

public class CombatVehicleManager {

    private ArrayList<CombatVehicle> vehicles;

    public CombatVehicleManager() {
        this.vehicles = new ArrayList<>();
    }

    public CombatVehicleManager(ArrayList<CombatVehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public CombatVehicleManager(Map<String, Integer> config)
    {
        this.vehicles = new ArrayList<>();
        var cash = new CombatVehiclesCash();
        config.forEach((k,v)-> vehicles.addAll(cash.get(k, v)));
    }

    public ArrayList<CombatVehicle> findAll(Predicate<CombatVehicle> predicate) {
        var result = new ArrayList<CombatVehicle>();

        for (CombatVehicle vehicle : vehicles) {
            if(predicate.test(vehicle))
            {
                result.add(vehicle);
            }
        }
        return result;
    }

    public static class MaxSpeedComparator implements Comparator<CombatVehicle> {

        @Override
        public int compare(CombatVehicle o1, CombatVehicle o2) {
            return Integer.compare(o1.getMaxSpeed(),o2.getMaxSpeed());
        }

    }
    public void sortByMaxSpeed(CmpOrder order) {
        vehicles.sort(new MaxSpeedComparator());
        if (order == CmpOrder.DSC) {
            Collections.reverse(vehicles);
        }    }

    class PersonnelCountComparator implements Comparator<CombatVehicle> {

        @Override
        public int compare(CombatVehicle o1, CombatVehicle o2) {
            return Integer.compare(o1.getPersonnelCount(),o2.getPersonnelCount());
        }

    }

    public void sortByPersonnelCount(CmpOrder order) {
        vehicles.sort(new PersonnelCountComparator());
        if (order == CmpOrder.DSC) {
            Collections.reverse(vehicles);
        }
    }



    public void sortByArmorType(CmpOrder order) {
        vehicles.sort( new Comparator<CombatVehicle>() {
            public int compare(CombatVehicle obj1, CombatVehicle obj2) {
                return obj1.getArmorType().compareTo(obj2.getArmorType());
            }
        });
        if (order == CmpOrder.DSC) {
            Collections.reverse(vehicles);
        }
    }


    public void sortByFuelConsumption(CmpOrder order) {
        vehicles.sort((v1, v2) -> v1.getFuelConsumption() - v2.getFuelConsumption());
        if (order == CmpOrder.DSC) {
            Collections.reverse(vehicles);
        }
    }

    public void shuffle() {
        Collections.shuffle(vehicles);
    }

    public ArrayList<CombatVehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(ArrayList<CombatVehicle> vehicles) {
        this.vehicles = vehicles;
    }



}
