package model;

public enum ChassisType {
    WHEELED,
    FULL_TRACK
}
