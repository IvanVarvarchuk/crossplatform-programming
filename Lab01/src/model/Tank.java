package model;

public class Tank extends CombatVehicle {
    public Weapon getWeapon() {
        return weapon;
    }

    private final Weapon weapon;

    public Tank(Tank tank) {
        super(tank);
        this.weapon = tank.weapon;
    }

    public Tank(String name, int maxSpeed, int fuelCapacity, int personnelCount, int fuelConsumption, ArmorType armorType, ChassisType chassisType, Weapon weapon) {
        super(name, maxSpeed, fuelCapacity, personnelCount, fuelConsumption, armorType, chassisType);
        this.weapon = weapon;
    }

    @Override
    public CombatVehicle clone() {
        return new Tank(this);
    }
/**/
    @Override
    public boolean equals(Object obj) {
        boolean result = super.equals(obj);
        Tank tank;
        if(obj instanceof Tank) {
            tank = (Tank) obj;
            return result &&
                weapon.equals(tank.getWeapon());
        }
        return result;
    }/**/
}

