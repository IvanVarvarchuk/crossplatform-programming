package model;

public class RecoveryVehicle extends CombatVehicle {

    private int cranPower;

    public RecoveryVehicle(RecoveryVehicle recoveryVehicle) {
        super(recoveryVehicle);
        this.cranPower = recoveryVehicle.cranPower;
    }

    public RecoveryVehicle(String name, int maxSpeed, int fuelCapacity, int personnelCount, int fuelConsumption,
                           ArmorType armorType, ChassisType chassisType, int cranPower) {
        super(name, maxSpeed, fuelCapacity, personnelCount, fuelConsumption, armorType, chassisType);
        this.cranPower = cranPower;
    }

    @Override
    public CombatVehicle clone() {
        return new RecoveryVehicle(this);
    }
    /**/
        @Override
        public boolean equals(Object obj) {
            var rv = (RecoveryVehicle)obj;
            return super.equals(obj) &&
                    cranPower == rv.getCranPower();
        }

     /**/
    public int getCranPower() {
        return cranPower;
    }

}
