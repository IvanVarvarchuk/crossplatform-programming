package com.lab02;

import java.util.*;
import java.io.*;
import java.util.stream.Collectors;

public class FCManager {
    private Map<String, List<FootballClub>> clubs;

    public FCManager() {
        clubs = new HashMap<>();
    }

    public Map<String, List<FootballClub>> getClubs() {
        return clubs;
    }

    public void setClubs(Map<String, List<FootballClub>> clubs) {
        this.clubs = clubs;
    }

    public void load_FromFile(String path) {
        clubs = loadFromFile(path);

    }
    public static HashMap<String, List<FootballClub>> loadFromFile(String path) {
        var map = new  HashMap<String, List<FootballClub>>();
        try (BufferedReader buffer = new BufferedReader(new FileReader(path))) {
            var lines = buffer.lines().collect(Collectors.toList());
            var fclubs = new ArrayList<FootballClub>();
            for (var line : lines) {

                var attr = line.split(" ");
                fclubs.add(new FootballClub(attr[0], attr[1], Integer.parseInt(attr[2])));

            }
            map = (HashMap<String, List<FootballClub>>) fclubs.stream().collect(Collectors.groupingBy(FootballClub::getCity));


        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }

        return map;
    }

    public static String lodFromFilesJoint(String firstPath, String secondPath) {
        var response = new StringBuilder();
        var firstMap = new HashMap<String, List<FootballClub>>();
        var secondMap = new HashMap<String, List<FootballClub>>();
        var resulted = new HashMap<String, List<FootballClub>>();

        response.append("\nCollection from first file:\n\n");
        firstMap = loadFromFile(firstPath);
        response.append(printClubs(firstMap));
        response.append("\nCollection from second file:\n\n");
        secondMap = loadFromFile(secondPath);
        response.append(printClubs(secondMap));

        for (Map.Entry<String, List<FootballClub>>  item : firstMap.entrySet()) {
            if(secondMap.containsKey(item.getKey()) && secondMap.containsValue(item.getValue()))
                resulted.put(item.getKey(),item.getValue());
        }

        response.append("\nMerged collection\n\n");
        response.append(printClubs(resulted));
        return response.toString();
    }

    public String findCommonCity() {
        var response = new StringBuilder();

        response.append("Common clubs in different cities:\n");
        var common = (HashMap<String, List<FootballClub>>) clubs.values().stream()
                .flatMap(Collection::stream).collect(Collectors.groupingBy(FootballClub::getName));

        for(Map.Entry<String, List<FootballClub>> item : common.entrySet()){
            if(item.getValue().size() >= 2){
                response.append("Clubs with the same name  ").append(item.getKey()).append("\n");
                for(FootballClub club : item.getValue()){
                    response.append(club).append("\n");
                }
            }
        }

        return response.toString();
    }


    public static String printClubs(Map<String, List<FootballClub>> map) {
        var response = new StringBuilder();

        for (Map.Entry<String, List<FootballClub>> clubSet : map.entrySet()) {
            response.append(String.format("Producer city: %s Football Clubs:\n", clubSet.getKey()));
            clubSet.getValue().forEach(v -> response.append(v).append("\n"));
        }
        return response.toString();
    }


    public String printFirstClubs(int n) {
        var response = new StringBuilder();

        for (Map.Entry<String, List<FootballClub>> clubSet : clubs.entrySet()) {
            response.append(String.format("Producer city: %s Football Clubs:\n", clubSet.getKey()));
            clubSet.getValue().stream().limit(n).forEach(v -> response.append(v).append("\n"));
        }
        return response.toString();
    }
}


