package com.lab02;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1 - Read map from file");
        System.out.println("2 - Print n first clubs in every entry from map");
        System.out.println("3 - Find amount of common clubs in different city");
        System.out.println("4 - Merge 2 maps from different files");
        System.out.println("5 - Exit");

        FCManager manager = new FCManager();
        int choice;

        while((choice = scanner.nextInt()) != 5){
            switch (choice) {
                case 1:
                    System.out.println("Path to file:");
                    var path = scanner.next();
                    manager.load_FromFile(path);
                    System.out.println(FCManager.printClubs(manager.getClubs()));

                    break;
                case 2:
                    System.out.println("count:");
                    int n = scanner.nextInt();
                    System.out.println(manager.printFirstClubs(n));
                    break;
                case 3:
                    System.out.println(manager.findCommonCity());
                    break;
                case 4:
                    System.out.println("Path to first file:");
                    var firstPath = scanner.next();
                    System.out.println("Path to second file:");
                    var secondPath = scanner.next();
                    System.out.println(FCManager.lodFromFilesJoint(firstPath, secondPath));
                    break;
                default:

            }
        }
    }
}
