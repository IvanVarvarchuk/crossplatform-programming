package com.lab02;

public class FootballClub {

    private String name;
    private String city;
    private int foundationYear;

    public FootballClub(String name, String city, int foundationYear) {
        this.name = name;
        this.city = city;
        this.foundationYear = foundationYear;
    }

    @Override
    public String toString() {
        return "FootballClub{" +
                "club name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", foundationYear=" + foundationYear +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getFoundationYear() {
        return foundationYear;
    }
}
