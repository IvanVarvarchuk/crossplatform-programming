package com.multythreads;
import java.io.Console;
import java.util.Scanner;

public class Main {
    public static  void main (String[] args) throws InterruptedException {
        int N = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter dimension of matrix (dimension must be even, positive, non zero): ");
        N = scanner.nextInt();

        System.out.print("\n");

        double[][] A = new double[N][N];
        for(int i = 0; i < N; ++i)
        {
            System.out.format("Enter %d row elements: ",i + 1);
            for(int j = 0; j < N; ++j)
            {
                A[i][j] = scanner.nextDouble();
            }
        }

        System.out.format("Enter solutions: ");
        double[] B = new double[N];
        for(int i = 0; i < N; ++i)
        {
            B[i] = scanner.nextDouble();
        }

        System.out.format("Multi-thread?: ");

        if(scanner.next().equals("Y"))
        {
            double[][] a = new double[N / 2][N / 2];
            double[][] a2 = new double[N / 2][N / 2];
            double[] b = new double[N / 2];
            double[] b2 = new double[N / 2];

            for (int i = 0; i < N / 2; ++i) {
                b[i] = B[i];
                b2[i] = B[i + N / 2];
                for (int j = 0; j < N / 2; ++j) {
                    a[i][j] = A[i][j];
                    a2[i][j] = A[i + N / 2][j + N / 2];
                }
            }

            System.out.println("\nFirst matrix:");
            printMatrix(a);
            System.out.println("\nSecond matrix:");
            printMatrix(a2);

            GaussAlgorithm gaussAlgorithm = new GaussAlgorithm(a, b);
            GaussAlgorithm gaussAlgorithm2 = new GaussAlgorithm(a2, b2);
            Thread gauss = new Thread(gaussAlgorithm);
            Thread gauss2 = new Thread(gaussAlgorithm2);

            Thread monitor = new Thread(new Monitor(gauss));
            Thread monitor1 = new Thread(new Monitor(gauss2));
            monitor.start();
            monitor1.start();
            monitor.join();
            monitor1.join();

            double timeElapsed = System.currentTimeMillis();
            gauss.start();
            gauss2.start();
            gauss.join();
            gauss2.join();
            timeElapsed -= System.currentTimeMillis();

            System.out.println("\nResult:");
            printVector(gaussAlgorithm.X);
            printVector(gaussAlgorithm2.X);

            System.out.println("\nElapsed Time: " + Math.abs(timeElapsed/1000.0));
        }
        else
        {
            GaussAlgorithm gaussAlgorithm = new GaussAlgorithm(A,B);

            Thread gauss = new Thread(gaussAlgorithm);
            Thread monitor = new Thread(new Monitor(gauss));

            monitor.start();

            double timeElapsed = System.currentTimeMillis();
            gauss.start();
            gauss.join();
            timeElapsed -= System.currentTimeMillis();

            System.out.println("\nResult:");
            printVector(gaussAlgorithm.X);

            System.out.println("\nElapsed Time: " + Math.abs(timeElapsed/1000.0));
        }


    }

    public static void printMatrix(double[][] m)
    {
        for(int i = 0; i < m.length; ++i)
        {
            for(int j = 0; j < m[i].length; ++j)
            {
                System.out.format("%.2f \t", m[i][j]);
            }
            System.out.print("\n");
        }
    }

    public static void printVector(double[] m)
    {
        for(int i = 0; i < m.length; ++i)
        {
            System.out.format("%.2f \t", m[i]);
        }
    }
}
