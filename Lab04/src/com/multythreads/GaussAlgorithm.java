package com.multythreads;

public class GaussAlgorithm implements Runnable
{
    private double[][] A;
    private double[] B;
    public  double[] X;

    GaussAlgorithm(double[][] A, double[] B)
    {
        this.A = A;
        this.B = B;
        X = new double[B.length];
    }

    @Override
    public void run()
    {
        int N = B.length;

        for(int row = 0; row < N; ++row)
        {
            int rowWithMaxFromColumn = row;

            for(int i = row+1; i < N; i++)
            {
                if(Math.abs(A[i][row]) > Math.abs(A[rowWithMaxFromColumn][row]))
                {
                    rowWithMaxFromColumn = i;
                }
            }

            double[] temp = A[row]; A[row] = A[rowWithMaxFromColumn]; A[rowWithMaxFromColumn] = temp;
            double t = B[row]; B[row] = B[rowWithMaxFromColumn]; B[rowWithMaxFromColumn] = t;

            for(int i = row + 1; i < N; i++)
            {
                double alpha = A[i][row] / A[row][row];
                B[i] -= alpha *B[row];
                for(int j = row; j < N; j++)
                {
                    A[i][j] -= alpha * A[row][j];
                }
            }
        }

        for(int i = N-1; i >= 0; i--)
        {
            double sum = 0.0;
            for (int j = i+1; j < N; j++)
            {
                sum += A[i][j] * X[j];
            }
            X[i] = (B[i] - sum)/ A[i][i];
        }
    }
}

