package com;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)throws Exception {
        File fileToRead = new File("Text.txt");
        Scanner scnr = new Scanner(fileToRead);

        ArrayList<String> lines = new ArrayList<>();
        ArrayList<String> linesToUp = new ArrayList<>();

        String text = "";
        while(scnr.hasNextLine()){
            String readedLine = scnr.nextLine();
            text = text + readedLine + '\n';
        }

        System.out.println(text);
        TextEditor.replace(text, '-', new String[]{"dd","mm","yyyy"});
        TextEditor.replaceLastCharacter(text);


    }
}
