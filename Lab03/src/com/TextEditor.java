package com;


import java.text.SimpleDateFormat;
import java.util.*;

public class TextEditor {

    public static ArrayList<String> split(String text){
        ArrayList<String> list = new ArrayList<>();

        while (true){
            int indexSpace = text.indexOf(' ');
            int indexN = text.indexOf('\n');

            String word = "";
            if(indexN!= -1 && indexSpace ==-1)
            {
                word = text.substring(0,indexN);
                text = text.substring(indexN + 1,text.length());
            }
            else{
                if (indexN < indexSpace){
                    word = text.substring(0,indexN);
                    text = text.substring(indexN + 1,text.length());
                }
                else{
                    word = text.substring(0,indexSpace);
                    text = text.substring(indexSpace + 1,text.length());
                }
            }

            if (word != ""){
                list.add(word);
                word = "";
            }
            if (indexSpace == -1) {
                break;
            }
        }

        return list;
    }

    public static ArrayList<String> splitByDot(String text){
        ArrayList<String> list = new ArrayList<>();

        while (true) {
            int index = text.indexOf('.');
            if (index == -1){
                break;
            }

            list.add(text.substring(0, index));
            text = text.substring(index + 2,text.length());


        }
        return list;
    }


    public static ArrayList<String> getDates (ArrayList<String> words, char splitter){
        ArrayList<String> res = new ArrayList<>();

        for (String word : words){
            if (word.indexOf(splitter) == 2 && word.lastIndexOf(splitter) == 5){

                String day = word.substring(0,2);
                String month = word.substring(3,5);
                String year = word.substring(6);


                int dayInt = -1;
                int monthInt = -1;
                int yearInt = -1;

                try {
                    dayInt = Integer.parseInt(day);
                    monthInt = Integer.parseInt(month);
                    yearInt = Integer.parseInt(year);
                }
                catch (NumberFormatException e) {
                    continue;
                }

                if (((dayInt >= 1) && (dayInt <= 31)) && ((monthInt >= 1) && (monthInt <= 12)) && (yearInt >= 1)){
                    String resDate = day + splitter + month + splitter + year;
                    res.add(resDate);
                }
            }
        }

        return res;
    }

    public static ArrayList<String> getDatesBy (ArrayList<String> words, char splitter, String[] array){
        ArrayList<String> res = new ArrayList<>();


        String dateFormat = array[0] + splitter + array[1] + splitter + array[2];
        int indexOfFirstSplitter = dateFormat.indexOf(splitter);
        int indexOfSecondSplitter = dateFormat.lastIndexOf(splitter);


        for (String word : words){
            if (word.indexOf(splitter) == indexOfFirstSplitter && word.lastIndexOf(splitter) == indexOfSecondSplitter){

                DateTuple firstNum = new DateTuple(0,word.substring(0, indexOfFirstSplitter));
                DateTuple secondNum = new DateTuple(0,word.substring(indexOfFirstSplitter + 1,indexOfSecondSplitter));
                DateTuple thirdNum = new DateTuple(0,word.substring(indexOfSecondSplitter + 1));

                HashMap<String, DateTuple> dictionary = new HashMap<String, DateTuple>();

                int[] num = new int[3];
                try {

                    firstNum.DateInt = Integer.parseInt(firstNum.DateString);
                    secondNum.DateInt = Integer.parseInt(secondNum.DateString);
                    thirdNum.DateInt = Integer.parseInt(thirdNum.DateString);

                    dictionary.put(array[0],firstNum);
                    dictionary.put(array[1],secondNum);
                    dictionary.put(array[2],thirdNum);

                }
                catch (NumberFormatException e) {
                    continue;
                }

                if (((dictionary.get("dd").DateInt >= 1)
                        && (dictionary.get("dd").DateInt <= 31))
                        && ((dictionary.get("mm").DateInt >= 1) && (dictionary.get("mm").DateInt <= 12))
                        && (dictionary.get("yyyy").DateInt >= 1)){
                    String resDate = dictionary.get(array[0]).DateString + splitter + dictionary.get(array[1]).DateString + splitter + dictionary.get(array[2]).DateString;
                    res.add(resDate);
                }
            }
        }

        return res;
    }

    public static ArrayList<String> getDatesByFromLines (ArrayList<String> lines, ArrayList<String> linesToUp, char splitter, String[] array){
        ArrayList<String> res = new ArrayList<>();


        String dateFormat = array[0] + splitter + array[1] + splitter + array[2];
        int indexOfFirstSplitter = dateFormat.indexOf(splitter);
        int indexOfSecondSplitter = dateFormat.lastIndexOf(splitter);

        for (String line : lines) {
            ArrayList<String> words = new ArrayList<>(Arrays.asList(line.split(" ")));
            for (String word : words){
                if (word.indexOf(splitter) == indexOfFirstSplitter && word.lastIndexOf(splitter) == indexOfSecondSplitter){

                    DateTuple firstNum = new DateTuple(0,word.substring(0, indexOfFirstSplitter));
                    DateTuple secondNum = new DateTuple(0,word.substring(indexOfFirstSplitter + 1,indexOfSecondSplitter));
                    DateTuple thirdNum = new DateTuple(0,word.substring(indexOfSecondSplitter + 1));

                    HashMap<String, DateTuple> dictionary = new HashMap<String, DateTuple>();

                    int[] num = new int[3];
                    try {

                        firstNum.DateInt = Integer.parseInt(firstNum.DateString);
                        secondNum.DateInt = Integer.parseInt(secondNum.DateString);
                        thirdNum.DateInt = Integer.parseInt(thirdNum.DateString);

                        dictionary.put(array[0],firstNum);
                        dictionary.put(array[1],secondNum);
                        dictionary.put(array[2],thirdNum);
                        linesToUp.add(line);

                    }
                    catch (NumberFormatException e) {
                        continue;
                    }

                    if (((dictionary.get("dd").DateInt >= 1)
                            && (dictionary.get("dd").DateInt <= 31))
                            && ((dictionary.get("mm").DateInt >= 1) && (dictionary.get("mm").DateInt <= 12))
                            && (dictionary.get("yyyy").DateInt >= 1)){
                        String resDate = dictionary.get(array[0]).DateString + splitter + dictionary.get(array[1]).DateString + splitter + dictionary.get(array[2]).DateString;
                        res.add(resDate);
                    }
                }
            }
        }
        return res;
    }

    public static Date getMinDate(ArrayList<String> dates, String format)throws Exception{
        ArrayList<Date> dates1 = new ArrayList<>();
        for (String s :dates) {
            dates1.add(new SimpleDateFormat(format).parse(s));
        }
        return Collections.min(dates1);
    }

    public static Date getMaxDate(ArrayList<String> dates, String format)throws Exception{
        ArrayList<Date> dates1 = new ArrayList<>();
        for (String s :dates) {
            dates1.add(new SimpleDateFormat(format).parse(s));
        }
        return Collections.max(dates1);
    }

    public static String getMidDate(Date d1, Date d2, String format){
        Date d3 = new Date((d1.getTime() + d2.getTime()) / 2);

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(d3);
    }

    public static void replace(String text,char splitter, String[] arrayFormat) throws Exception{
        ArrayList<String> lines = TextEditor.splitByDot(text);
        ArrayList<String> linesToUp = new ArrayList<>();

        ArrayList<String> dates = TextEditor.getDatesByFromLines(lines, linesToUp, splitter, arrayFormat);
        String format = arrayFormat[0]+splitter+arrayFormat[1]+splitter+arrayFormat[2];
        String midDate = getMidDate(
                getMinDate(dates, format)
                ,getMaxDate(dates, format)
                ,format);

        System.out.println(midDate);


        for (String l: linesToUp){
            System.out.println(l);
        }
        System.out.println(text);


        StringBuilder textS = new StringBuilder(text);
        for (String lineToUp: linesToUp){
            int startIndex = text.indexOf(lineToUp);
            while (textS.charAt(startIndex) != '.'){
                textS.setCharAt(startIndex,Character.toUpperCase(textS.charAt(startIndex)));
                startIndex++;
            }
        }

        System.out.println(textS);

        StringBuilder textSToSearch = new StringBuilder(text);

        for (String date: dates){
            while (true){
                int startIndex = textSToSearch.indexOf(date);
                if (startIndex == -1) {
                    break;
                }
                if (midDate.compareTo(date) == 0){
                    for (int i=0; i < 10;i++){
                        textSToSearch.setCharAt(startIndex,'-');
                        startIndex++;
                    }
                    continue;
                }

                for (int i=0; i < 10;i++){
                    textS.setCharAt(startIndex,midDate.charAt(i));
                    textSToSearch.setCharAt(startIndex,midDate.charAt(i));
                    startIndex++;
                }
            }

        }

        System.out.println(textS);

    }


    public static void replaceLastCharacter(String text){
        ArrayList<String> words = new ArrayList<String>(Arrays.asList(text.split("\\s*(\\s|,|!|\\.)\\s*")));

        StringBuilder textS = new StringBuilder(text);

        for (String word: words){
            text = text.replaceAll("\\b(?:"+word+")\\b", word.substring(0, word.length()-1) + word.substring(word.length() - 1).toUpperCase());
        }

        System.out.println(text);
    }


}
